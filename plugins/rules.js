export default {
  required: [
    v => !!v || 'This field is required'
  ]
}