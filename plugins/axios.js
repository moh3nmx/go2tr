export default function ({ $toast, $axios, redirect }) {
  $axios.onError(error => {
    $toast.error(error)
  })
}