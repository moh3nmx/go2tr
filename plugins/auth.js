export default function ({ app }) {
  if (app.$auth.loggedIn && !app.$auth.user.username) {
    let token = app.$auth.strategy.token.get()
    if (token.includes('admin')) {
      app.$auth.setUser({
        id: 1,
        username: 'admin',
        role: 1
      })
    } else if (token.includes('user')) {
      app.$auth.setUser({
        id: 2,
        username: 'user',
        role: 2
      })
    } else {
      app.$auth.logout()
      app.$router.push('/login')
    }
  }
}