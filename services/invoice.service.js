import api from './endpoints'

export default function ({ app, store }, inject) {
  const methods = {
    getInvoices() {
      return app.$axios(api.invoice.list)
    },
    updateInvoice(data) {
      return app.$axios({
        ...api.invoice.update(data.id),
        data
      })
    },
    createInvoice(data) {
      return app.$axios({
        ...api.invoice.create,
        data
      })
    }
  }


  // inject into Vue Proto
  inject('invoice', methods)
}
