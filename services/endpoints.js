export default {
  auth: {
    update_user: (id) => ({
      url: '/api/v1/updateuser/' + id,
      method: 'put'
    })
  },
  invoice: {
    list: {
      url: '/api/v1/invoice',
      method: 'get'
    },
    single: (id) => ({
      url: '/api/v1/invoice/' + id,
      method: 'get'
    }),
    update: (id) => ({
      url: '/api/v1/invoice/' + id,
      method: 'put'
    }),
    create:{
      url: '/api/v1/invoice',
      method: 'post'
    }
  }
}