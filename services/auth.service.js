import api from './endpoints'

export default function ({ app, store }, inject) {
  const methods = {
    login(data) {
      if (data.username == 'admin') {
        return {
          id: 1,
          username: 'admin',
          role: 1,
          token: 'admin_token'
        }
      } else if (data.username == 'user') {
        return {
          id: 2,
          username: 'user',
          role: 2,
          token: 'user_token'
        }
      } else {
        throw new Error('Invalid Username or password')
      }
    },
    updateUser(data) {
      return app.$axios(
        {
          ...api.auth.update_user(data.id),
          data
        }
      )
    }
  }


  // inject into Vue Proto
  inject('authentication', methods)
}




