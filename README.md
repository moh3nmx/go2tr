# Go2TR

> GO2TR Test Case

## Login

Authentication service is hard code, so to authenticate you need to use these usernames:

- user
- admin

and enter a string for password.

## Build Setup

```bash

# install dependencies

$ npm install



# serve with hot reload at localhost:3000

$ npm run dev



# build for production and launch server

$ npm run build

$ npm start



# generate static project

$ npm run generate

```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).
