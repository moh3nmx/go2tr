export default function ({ store, route }) {
  if (route.meta[0].title) store.commit('setPageTitle', route.meta[0].title)
  else store.commit('setPageTitle', '')
}