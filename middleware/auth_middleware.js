export default function ({ store, redirect, route }) {
  if (!store.state.auth.loggedIn) {
    redirect('/login')
  } else {
    const user = store.state.auth.user
    if (route.meta[0].role && user.role > route.meta[0].role) {
      redirect('/settings')
    }
  }
}